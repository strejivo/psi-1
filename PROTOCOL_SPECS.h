/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PROTOCOL_SPECS.h
 * Author: strejivo
 *
 * Created on February 25, 2017, 9:53 PM
 */

#ifndef PROTOCOL_SPECS_H
#define PROTOCOL_SPECS_H

/*

 * PREDEFINING MESSAGES, THAT HAVE GIVEN FORM. MESSAGES CONTAINING CUSTOM INFO IS NOT INCLUDED HERE. 
 
 */

#define SERVER_USER "100 LOGIN\r\n"
#define SERVER_PASSWORD	"101 PASSWORD\r\n"
#define SERVER_MOVE "102 MOVE\r\n"
#define SERVER_TURN_LEFT "103 TURN LEFT\r\n"
#define SERVER_TURN_RIGHT "104 TURN RIGHT\r\n"
#define SERVER_PICK_UP "105 GET MESSAGE\r\n"
#define SERVER_OK "200 OK\r\n"
#define SERVER_LOGIN_FAILED "300 LOGIN FAILED\r\n"
#define SERVER_SYNTAX_ERROR "301 SYNTAX ERROR\r\n"
#define SERVER_LOGIC_ERROR "302 LOGIC ERROR\r\n"

#define CLIENT_RECHARGING "RECHARGING\r\n"
#define CLIENT_FULL_POWER "FULL POWER\r\n"

#define TIMEOUT 1
#define TIMEOUT_RECHARGING 5

// MAX LENGTHS OF CUSTOM MESSAGES
#define CLIENT_USER_MAXLENGTH 100
#define CLIENT_PASSWORD_MAXLENGTH 7
#define CLIENT_CONFIRM_MAXLENGTH 12
#define CLIENT_MESSAGE_MAXLENGTH 100

#endif /* PROTOCOL_SPECS_H */

