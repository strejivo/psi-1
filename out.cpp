/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   PROTOCOL_SPECS.h
 * Author: strejivo
 *
 * Created on February 25, 2017, 9:53 PM
 */

#ifndef PROTOCOL_SPECS_H
#define PROTOCOL_SPECS_H

/*

 * PREDEFINING MESSAGES, THAT HAVE GIVEN FORM. MESSAGES CONTAINING CUSTOM INFO IS NOT INCLUDED HERE. 
 
 */

#define SERVER_USER "100 LOGIN\r\n"
#define SERVER_PASSWORD	"101 PASSWORD\r\n"
#define SERVER_MOVE "102 MOVE\r\n"
#define SERVER_TURN_LEFT "103 TURN LEFT\r\n"
#define SERVER_TURN_RIGHT "104 TURN RIGHT\r\n"
#define SERVER_PICK_UP "105 GET MESSAGE\r\n"
#define SERVER_OK "200 OK\r\n"
#define SERVER_LOGIN_FAILED "300 LOGIN FAILED\r\n"
#define SERVER_SYNTAX_ERROR "301 SYNTAX ERROR\r\n"
#define SERVER_LOGIC_ERROR "302 LOGIC ERROR\r\n"

#define CLIENT_RECHARGING "RECHARGING\r\n"
#define CLIENT_FULL_POWER "FULL POWER\r\n"

#define TIMEOUT 1
#define TIMEOUT_RECHARGING 5

// MAX LENGTHS OF CUSTOM MESSAGES
#define CLIENT_USER_MAXLENGTH 100
#define CLIENT_PASSWORD_MAXLENGTH 7
#define CLIENT_CONFIRM_MAXLENGTH 12
#define CLIENT_MESSAGE_MAXLENGTH 100

#endif /* PROTOCOL_SPECS_H */

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <string>
#include <cstring>
#include <queue>

#include <unistd.h>
#include <netdb.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <pthread.h>
#include <thread>
#include "PROTOCOL_SPECS.h"

#define MAX_QUE_CLIENTS 5
using namespace std;

class socketStream{
public:
    socketStream(int rs) : socket(rs){
        FD_ZERO(&readfds);
        FD_SET(socket, &readfds);
    }
    void send(const char * message){
            if(::send(socket, message, strlen(message), 0) == -1) {
            throw "COULD NOT SEND";
        }
    }
    void read(int maxLen, string & str, bool shorten = true, int timeout = 1){
            int loaded = 0;
            str = "";
            bool cr = false; // carriage return
            while(loaded < maxLen){
                if (buffer.size() == 0){
                    struct timeval tv;
                    tv.tv_sec =  timeout;
                    tv.tv_usec = 0;
                    unsigned char bf[1024];
                    int retVal = select(socket+1, &readfds, NULL, NULL, &tv);
                    if(retVal <= 0) {
                        throw "TIMEOUT";
                    }
                    int ln = recv(socket, bf, 1024, 0);
                    for ( int i = 0; i < ln; i++) buffer.push(bf[i]);
                }
                unsigned char tmp = buffer.front();
                buffer.pop();
                str.push_back(tmp);
                loaded++;
                if (tmp == '\r') cr = true;
                else if ( tmp == '\n' && cr) {
                    if ( str == CLIENT_RECHARGING ){
                        read(strlen(CLIENT_FULL_POWER), str, false, 5);
                        if(str != CLIENT_FULL_POWER ){
                            send(SERVER_LOGIC_ERROR);
                            throw "LOGIC ERROR";
                        }else loaded = 0;
                        cr = false;
                        str = "";
                        continue;
                    }
                    if(shorten){
                        str.pop_back();
                        str.pop_back();
                    }
                    return;
                }
                else cr = false;
            }
            send(SERVER_SYNTAX_ERROR);
            throw "MESSAGE TOO LONG";
            return;
    }
private:
    fd_set readfds;
    queue<unsigned char> buffer;
    int socket;
};

class Robot{
public:
    Robot(int sock) : sStream(sock), socket(sock){
    }
    
    /**
     * Authenticates the robot
     */
    void authenticate(){
        string buff;
        int expectedPasswd = 0; //Expected password
        bool passwdError =  false;
        int recPasswd = 0; //Received password
        sStream.send(SERVER_USER);
        //RECEIVE LOGIN FROM BOT, ADD UP THE ASCII VALUES
        sStream.read(max((int)strlen(CLIENT_RECHARGING), CLIENT_USER_MAXLENGTH), buff);
        for(auto it : buff){
            expectedPasswd+=(unsigned char)it;
        }
        //SEND PASSWORD REQUESTs
        sStream.send(SERVER_PASSWORD);

        sStream.read(max((int)strlen(CLIENT_RECHARGING), CLIENT_PASSWORD_MAXLENGTH), buff);
        if (buff.length() > 5){
            sStream.send(SERVER_SYNTAX_ERROR);
            throw "SYNTAX ERROR";
        }

        for(auto it : buff){
            if ( it < '0' || it > '9'){
                passwdError = true;
                break;
            }
            recPasswd*=10;
            recPasswd+=((unsigned char)it)-('0');
        }
        if (passwdError){
            sStream.send(SERVER_SYNTAX_ERROR);
            throw "BAD PASSWORD FORMAT";
        }
        if ( expectedPasswd != recPasswd ){
            sStream.send(SERVER_LOGIN_FAILED);
            throw "BAD LOGIN";
        }
        sStream.send(SERVER_OK);
    }
    
    /**
     * Navigates the robot to home ( home is considered [0,0] )
     */
    void navigateHome(){
        turnLeft();
        if(posX == 0 && posY == 0){
            return;
        }

        int oldX = posX, oldY = posY;
        goOne();
        if(posX > oldX){
            if (posX > 0){
                if ( posY > 0){
                    turnRight();
                    while(posY != 0) goOne();
                    turnRight();
                }
                else{
                    turnLeft();
                    while(posY != 0) goOne();
                    turnLeft();
                }
                while(posX != 0) goOne();
            }
            else{
                while(posX != 0) goOne();
                if (posY > 0) turnRight();
                else turnLeft();
                while(posY != 0) goOne();
            }
        }
        else if(posX < oldX) {
            if (posX < 0){
                if ( posY < 0){
                    turnRight();
                    while(posY != 0)goOne();
                    turnRight();
                }
                else{
                    turnLeft();
                    while(posY != 0)goOne();
                    turnLeft();
                }
                while(posX != 0) goOne();
            }
            else{
                while(posX != 0) goOne();
                if (posY < 0) turnRight();
                else turnLeft();
                while(posY != 0) goOne();
            }
        }
        else if(posY > oldY) {
            if(posY > 0){
                if(posX > 0) {
                    turnLeft();
                    while(posX != 0)goOne();
                    turnLeft();
                }
                else{
                    turnRight();
                    while(posX != 0) goOne();
                    turnRight();
                }
                while(posY != 0) goOne();
            }else{
                while(posY != 0) goOne();
                if(posX > 0) turnLeft();
                else turnRight();
                while(posX != 0) goOne();
            }
        }
        else {
            if(posY < 0){
                if(posX < 0) {
                    turnLeft();
                    while(posX != 0) goOne();
                    turnLeft();
                }
                else{
                    turnRight();
                    while(posX != 0) goOne();
                    turnRight();
                }
                while(posY != 0) goOne();
            }else{
                while(posY != 0) goOne();
                if(posX > 0) turnRight();
                else turnLeft();
                while(posX != 0) goOne();
            }
        }
    }
    
    /**
     * Pick up the message
     */
    void pickUpMessage(){
        string buff;
        sStream.send(SERVER_PICK_UP);
        sStream.read(CLIENT_MESSAGE_MAXLENGTH, buff);
        cout<<buff<<endl;
        sStream.send(SERVER_OK);
    }
private:
    int posX, posY; /** < positins X and Y of the robot > */ 
    socketStream sStream; /** < socket stream used to communicate with the given bot > */
    int socket; /** < socket, that is used to communicate with the bot > */
    
    /**
     * Reads OK message from robot and extracts the coordinates
     * @param x reference to where we want to store X coordinate
     * @param y reference to where we want to store Y coordinate
     */
    void getCoords(int & x, int & y){
        string buff;
        char c;
        sStream.read(CLIENT_CONFIRM_MAXLENGTH, buff);
        if ( buff.length() < 6 || buff.substr(0, 3) != "OK "){
            sStream.send(SERVER_SYNTAX_ERROR);
            throw "WRONG OK FORMAT";
        }
        buff = buff.substr(3);
        stringstream ss(buff);
        if(!(ss>>noskipws>>x>>c>>y) || c!=' ' || !ss.eof()) {
            sStream.send(SERVER_SYNTAX_ERROR);
            throw "WRONG OK FORMAT";
        }
    }
    
    /**
     * Moves the robot by one, repeats until he really moves
     */
    void goOne(){
        int newX = posX, newY = posY;
        while( newX == posX && newY == posY) {
            sStream.send(SERVER_MOVE);
            getCoords(newX, newY);
        }
        posX = newX;
        posY = newY;
    }

   /**
    * Turn the robot left
    */
    void turnLeft(){
        sStream.send(SERVER_TURN_LEFT);
        getCoords(posX, posY);
    }

    /**
     * Turns the robot right
     */
    void turnRight(){
        sStream.send(SERVER_TURN_RIGHT);
        getCoords(posX, posY);
    }
};

/**
 * Create new server socket, that will be binded to "name" network and port and will listen
 * @param name network
 * @param port port
 * @return -1 for fail, else socket id
 */

int createServerSocket(string name, string port){
    int sock;
    if ((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) return -1;
    struct addrinfo * ai;
    if (getaddrinfo(name.c_str(), port.c_str(), NULL, &ai) != 0){
        close(sock);
        return -1;
    }
    if (bind(sock, ai->ai_addr, ai->ai_addrlen) == -1) {
        close(sock);
        freeaddrinfo(ai);
        return -1;
    }
    // listen, prijme max 5 clenu do rady, pak je bude odmitat
    if (listen(sock, MAX_QUE_CLIENTS) == -1){
        close(sock);
        freeaddrinfo(ai);
        return -1;
    }
    freeaddrinfo(ai);
    return sock;
}

void serveRobot(int socket){
    try{
        Robot bot(socket);
        bot.authenticate();
        bot.navigateHome();
        bot.pickUpMessage();
        close(socket);
        return;
    } catch(const char * e) {
        cout<<e<<endl;
        close(socket);
        return;
    }
}

int main(int argc, char * argv[]) {
    int servSock;
    string port = "3535";
    if (argc > 1) port = argv[1];
    if ((servSock = createServerSocket("0.0.0.0",port.c_str())) == -1){
        cout<<"ERROR! COULD NOT CREATE SOCKET"<<endl;
        return 0;
    }
    while(1){
        int bot_sock;
        struct sockaddr_in bot_addr;
        socklen_t bot_addr_len = sizeof(bot_addr);
        bot_sock = accept(servSock, (struct sockaddr *)&bot_addr, &bot_addr_len);
        if(bot_sock <= -0) cout<<"ERROR! FAILED TO CONNECT WITH THE BOT"<<endl<<"CONTINUING"<<endl;
        else{
            thread x (serveRobot, bot_sock);
            x.detach();
        }
    }
    close(servSock);
    return 0;
}
